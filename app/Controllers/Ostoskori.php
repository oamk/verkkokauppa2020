<?php  namespace App\Controllers;

use App\Models\TuoteryhmaModel;
use App\Models\OstoskoriModel;

/**
 * Ostoskori-toiminnallisuuden käsittelijä.
 */
class Ostoskori extends BaseController
{
  private $tuoteryhmaModel=null;
  private $ostoskoriModel=null;

  public function __construct() {
    $this->tuoteryhmaModel = new TuoteryhmaModel();
		$this->ostoskoriModel = new OstoskoriModel();
	}

	/**
	 * Näyttää ostoskorin sisällön.
	 */
  public function index()
	{
		$data['tuoteryhmat'] = $this->tuoteryhmaModel->haeTuoteryhmat();
		// Istuntomuuttuja sisältää suoraan taulukon tuotteita eli ei ole tarvetta hakea mitään 
		// tietoja tietokannasta.
    $data['tuotteet'] = $this->ostoskoriModel->ostoskori();
    $data['ostoskori_lkm'] = $this->ostoskoriModel->lukumaara();
		echo view('templates/header',$data );
		echo view('ostoskori',$data);
		echo view('templates/footer');
	}

	/**
	 * Tallentaa tilauksen.
	 */
	public function tilaa()
	{
		$data['tuoteryhmat'] = $this->tuoteryhmaModel->haeTuoteryhmat();
    $asiakas = [
      'etunimi' => $this->request->getPost('etunimi'),
      'sukunimi' => $this->request->getPost('sukunimi'),
      'lahiosoite' => $this->request->getPost('lahiosoite'),
      'postinumero' => $this->request->getPost('postinumero'),
      'postitoimipaikka' => $this->request->getPost('postitoimipaikka'),
      'email' => $this->request->getPost('email'),
      'puhelin' => $this->request->getPost('puhelin')
    ];

		$this->ostoskoriModel->tilaa($asiakas);

		$data['ostoskori_lkm'] = $this->ostoskoriModel->lukumaara();
    echo view('templates/header',$data);
		echo view('kiitos');
		echo view('templates/footer');
	}

	/**
	 * Lisää tuotteen ostoskoriin.
	 */
  public function lisaa($tuote_id) { 
		$this->ostoskoriModel->lisaa($tuote_id);
    return redirect()->to(site_url('/kauppa/tuote/' . $tuote_id));
  }

	/**
	 * Tyhjentää ostoskorin.
	 */
  public function tyhjenna() {
		$this->ostoskoriModel->tyhjenna();
    return redirect()->to(site_url('ostoskori/index'));		
	}
	
	/**
	 * Poistaa valitun tuotteen ostoskorista.
	 */
	public function poista($tuote_id) {
		$this->ostoskoriModel->poista($tuote_id);
		return redirect()->to(site_url('ostoskori/index'));	
	}

}