<?php namespace App\Controllers;

use App\Models\TuoteryhmaModel;
use App\Models\TuoteModel;

class Admin extends BaseController
{
  private $tuoteryhmaModel=null;
  private $tuoteModel=null;

  function __construct()
  {
    $this->tuoteryhmaModel = new TuoteryhmaModel();
    $this->tuoteModel = new TuoteModel();
  }

  /**
   * Näyttää tuoteryhmät (ylläpidon etusivun).
   */
	public function index()
	{
    $data['tuoteryhmat'] = $this->tuoteryhmaModel->haeTuoteryhmat();
    $data['otsikko'] = 'Tuoteryhmät';
    echo view('templates/header_admin.php');
    echo view('admin/tuoteryhma.php',$data);
    echo view('templates/footer.php'); 
	}

  /**
   * Tallentaa tuoteryhmän.
   * 
   * @param int $tuoteryhma_id = Tuoteryhmän id, mikäli tuoteryhmää muokataan.
   */
  public function tallenna($tuoteryhma_id = null) {
    // Näytetään otsikko sen mukaan, ollaanko lisäämässä vai muokkaamassa.
    if ($tuoteryhma_id != null || $this->request->getPost('id')!=null) {
      $data['otsikko'] = "Muokkaa tuoteryhmää";
    }
    else {
      $data['otsikko'] = "Lisää tuoteryhmä";
    }

    // Jos post-metodi, yritetään tallentaa.
    if ($this->request->getMethod() === 'post') {
      if (!$this->validate([
        'nimi' => 'required|max_length[50]'
      ])) {  
        // Validointi ei mene läpi, palautetaan lomake näkyviin.
        $data['id'] = $this->request->getPost('id');
        $data['nimi'] = $this->request->getPost('nimi');
        $this->naytaLomake($data);
      }
      else {
        // Tallennetaan.
        $talleta['id'] = $this->request->getPost('id');
        $talleta['nimi'] = $this->request->getPost('nimi');
        $this->tuoteryhmaModel->save($talleta);
        return redirect('admin/index');
      }
    }
    else {
      // Näytetään lomake.
      $data['id'] = '';
      $data['nimi'] = '';
      // Mikäli tuoteryhmä on asetettu, ollaan muokkaamassa ja haetaan tietokannasta
      // tiedot lomakkeelle.
      if ($tuoteryhma_id != null) {
        $tuoteryhma = $this->tuoteryhmaModel->hae($tuoteryhma_id);
        $data['id'] = $tuoteryhma['id'];
        $data['nimi'] = $tuoteryhma['nimi'];  
      }
      $this->naytaLomake($data);
    }
  }

  /**
   * Poistaa tuoteryhmän ja tuoteryhmään kuuluvat tuotteet. Mikäli tuoteryhmän tuotteita on tilattu, poistoa ei tehdä, vaan annetaan käyttäjälle ilmoitus, että
   * tilauksia on.
   * 
   * @param int $id Poistettavan tuoteryhmän id.
   */
  public function poista($id) {
    // Poisto suoritetaan try-catch -lauseen sisällä, koska tuoteryhmän poistamisessa voi tapahtua virhe, mikäli tuoteryhmän tuotteita on tilattu, 
    // jolloin poista ei onnistu.
    try {
      // Poistetaan ensin tuotteet tuoteryhmän alta.
      $this->tuoteModel->poistaTuoteryhmalla($id);
      // Poistetaan tuoteryhmä.
      $this->tuoteryhmaModel->poista($id);
      return redirect ('admin/index');
    }
    catch (\Exception $e) {
      // Tarkastetaan, johtuuko virhe siitä, että tuoteryhmän alla oleville tuotteille on tilauksia, jolloin poistaminen
      // ei onnistu.
      if ($e->getCode() === 1451) {
        $data['otsikko'] = 'Tuoteryhmää ei voida poistaa';
        $data['viesti'] = "Tuoteryhmään kuuluvia tuotteita on tilattu. Tuoteryhmää ei voida poistaa. Mikäli haluat poistaa tuoteryhmän,
        poista tilaukset ensin.";
        echo view('templates/header_admin.php');
        echo view('admin/ilmoitus.php',$data);
        echo view('templates/footer.php');
      }
      else { // Heitetään poikkeus edelleen, mikä aiheuttaa virheen näyttämisen.
        throw new $e;
      }
    }
  }

  /**
  * Näyttää tuoteryhmän lisäys/muokkauslomakkeen.
  *
  * @param Array $data Lomakkeelle välitettävät muuttujat taulukossa.
  */
  private function naytaLomake($data) {
    echo view('templates/header_admin.php');
    echo view('admin/tuoteryhma_lomake.php',$data);
    echo view('templates/footer.php');
  }


	//--------------------------------------------------------------------

}