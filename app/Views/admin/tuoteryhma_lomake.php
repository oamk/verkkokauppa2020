<h3><?= $otsikko?></h3>
<div>
  <?= \Config\Services::validation()->listErrors();?>
</div>
<form action="/admin/tallenna" method="post">
  <input type="hidden" name="id" value="<?= $id?>">
  <div>
    <label>Nimi</label>
    <input name="nimi" maxlength="50" value="<?= $nimi?>"/>
  </div>
  <button>Tallenna</button>
</form>