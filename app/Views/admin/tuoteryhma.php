<h3><?= $otsikko?></h3>
<div>
<?= anchor('admin/tallenna','Lisää uusi')?>
</div>
<table class="table">
<?php foreach($tuoteryhmat as $tuoteryhma): ?>
  <tr>
    <td><?= $tuoteryhma['nimi']?></td>
    <td><?= anchor('admin/tallenna/' . $tuoteryhma['id'],'Muokkaa')?></td>
    <!- Kysytään varmistus, tehdäänkö poisto. -->
    <td><a href="<?= site_url('admin/poista/'. $tuoteryhma['id'])?>" onclick="return confirm('Haluatko varmasti poistaa tuoteryhmän? Myös kaikki tuoteryhmän tuotteet poistetaan.')">Poista</a></td>
  </tr>
<?php endforeach;?>
</table>
