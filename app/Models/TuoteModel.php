<?php namespace App\Models;

use CodeIgniter\Model;

/**
 * Sisältää tuote-taulun käsittelyyn liittyviä metodeja.
 */
class TuoteModel extends Model {
  protected $table = 'tuote'; // Malli käsittelee tuote-taulua tietokannassa.

   // Luettelo niistä kentistä, joita päivitetään, kun ajetaan tallennus (esim. save) tietokantaan.
   protected $allowedFields = ['nimi','hinta','kuvaus','kuva','varastomaara','tuoteryhma_id'];

  /**
   * Hakee tuotteita tuoteryhmän perusteella.
   * 
   * @param int $tuoteryhma_id Tuoteryhmän id, jonka perusteella haetaan tuotteita. 
   * @return Array Haetut tuotteet taulukkona.
   */
  public function haeTuoteryhmalla($tuoteryhma_id) {
     return $this->getWhere(['tuoteryhma_id' => $tuoteryhma_id])->getResultArray();
  }


  /**
   * Arpoo kolme satunnaista tuotetta (sama tuote ei esiinny useammin taulukossa).
   * 
   * @return Array Kolme satunnaista tuotetta taulukossa.
   */
  public function haeKolmeSatunnaistaTuotetta() {
    $arvotut = array(); // Taulukko satunnaisesti arvotuille tuotteilla.
    $idt = array(); // Idt taulukkon tallennetaan jo arvottujen tuotteiden idt, jotta voidaan tarkastaa
                    // in_array-metodilla, onko tuote jo arvottu.
    $tuotteet = $this->findAll(); // Haetaan kaikki tuotteet tietokannasta.
    // Jos tuotteita on enemmän kuin kolme, voidaan arpoa tuotteista 3 satunnaista.
    if (count($tuotteet) > 3) {
      $tuotteita = 0;
      while ($tuotteita < 3) { // Arvotaan niin kauan, että kolme eri tuotetta on löytynyt.
        $tuote = $tuotteet[rand(0,count($tuotteet)-1)]; // Arvotaan tuote taulukosta välillä 0 - taulukon koko.
        if (!in_array($tuote['id'],$idt)) { // Tarkastetaan, että tuote ei ole jo arvottu.
          array_push($idt,$tuote['id']); // Lisätään id, jotta voidaan tarkastaa, onko tuote jo taulukossa.
          array_push($arvotut,$tuote); // Lisätään tuote taulukkoon.
          $tuotteita++; // Kasvatetaan arvottujen tuotteiden lukumäärää, jotta while loppuu joskus.
        }
      }
      return $arvotut; // Palautetaan arvotut.
    }
    else { // Tuotteita 3 tai vähemmän, joten palautetaan kaikki.
      return $tuotteet; // Palautetaan tuotteet suoraan, koska niitä on 3 tai vähemmän.
    }
  }

  /**
   * Hakee tuotteen id:n perusteella.
   * 
   * @param int $id Haettavan tuotteen id.
   * @return Array Haetun tuotteen tiedot taulukkona (yksi rivi).
   */
  public function hae($id) {
    $this->where('id',$id);
    $query = $this->get();
    $tuote = $query->getRowArray();
    // Voidaan käyttää debuggauksessa, kun halutaan tietää, mikä
    // kysely suoritettiin.
    //echo $this->getLastQuery(); 
    return $tuote;
  }

  /**
   * Hakee tuotteita id arvojen perusteella.
   * 
   * @param Array $idt Tuotteiden id(t) taulukossa.
   * @return Array Haetut tuotteet taulukossa.
   */
  public function haeTuotteet($idt) {
    $palautus = array();
    foreach ($idt as $id) {
      $this->table('tuote');
      $this->select('id,nimi,hinta');
      $this->where('id',$id);
      $query = $this->get();
      $tuote = $query->getRowArray();
      array_push($palautus,$tuote);
        
      $this->resetQuery();
    }
   
    return $palautus;
  }

  /**
   * Hakee tuotteita nimen perusteella.
   * 
   * @param $nimi Tuotteen nimi tai osa nimestä, jolla haetaan.
   * @return Array Nimen perusteella löydetyt tuotteet taulukossa. 
   */
  public function haeNimella($nimi) {
    $this->like('nimi',$nimi);
    $query = $this->get();
    return $query->getResultArray();
  }


   /**
   * Poistaa tuotteet tuoteryhmän alta.
   * 
   * @param int $tuoteryhma_id Tuoteryhmän id, jonka alta tuotteet poistetaan.
   */
  public function poistaTuoteryhmalla($tuoteryhma_id) {
    $this->where('tuoteryhma_id',$tuoteryhma_id);
    $this->delete();
  }
}