drop database if exists verkkokauppa;
create database verkkokauppa;
use verkkokauppa;

create table tuoteryhma (
  id int primary key auto_increment,
  nimi varchar(50) unique not null
);

create table tuote (
  id int primary key auto_increment,
  nimi varchar(50) not null,
  kuvaus text not null,
  hinta double(6,2) not null,
  kuva varchar(50),
  varastomaara smallint unsigned,
  tuoteryhma_id int not null,
  index tuoteryhma_id(tuoteryhma_id),
  foreign key (tuoteryhma_id) references tuoteryhma(id)
  on delete restrict
);


insert into tuoteryhma (id, nimi) values (1, 'Asusteet');
insert into tuoteryhma (id, nimi) values (2, 'Kengät');
insert into tuoteryhma (id, nimi) values (3, 'Urheiluvälineet');
insert into tuoteryhma (id, nimi) values (4, 'Pyörät');
insert into tuoteryhma (id, nimi) values (5, 'Kellot');


insert into tuote (nimi, kuvaus, hinta, kuva, varastomaara,tuoteryhma_id) 
values ('The North Face','Halo Down Hoodie, miesten untuvatakki',70,'takki.png',10,1);

insert into tuote (nimi, kuvaus, hinta, kuva, varastomaara,tuoteryhma_id) 
values ('Haglöfs','Vision II GTX, miesten retkeilykengät',60,'kengat.png',10,2);

insert into tuote (nimi, kuvaus, hinta, kuva, varastomaara,tuoteryhma_id) 
values ('Bauer','Aikuisten luistimet',25,'luistimet.jpg',20,3);

insert into tuote (nimi, kuvaus, hinta, kuva, varastomaara,tuoteryhma_id) 
values ('Ghost','Kato 2,9 19 unisex maastopyörä',399,'pyora.png',5,4);

insert into tuote (nimi, kuvaus, hinta, kuva, varastomaara,tuoteryhma_id) 
values ('White','XC 290 Pro 19, unisex maastopyörä',849,'pyora2.png',10,4);

insert into tuote (nimi, kuvaus, hinta, kuva, varastomaara,tuoteryhma_id) 
values ('Suunto','Suunto 5, urheilukello sykemittarilla',249,'kello.png',35,5);

create table asiakas (
  id int primary key auto_increment,
  sukunimi varchar(50) not null,
  etunimi varchar(50) not null,
  lahiosoite varchar(100) not null,
  postitoimipaikka varchar(50) not null,
  postinumero char(5) not null,
  puhelin varchar(20),
  email varchar(100)
);

create table tilaus (
  id int primary key auto_increment,
  paivays timestamp default current_timestamp,
  asiakas_id int not null,
  tila enum ('tilattu','toimitettu'),
  index asiakas_id(asiakas_id),
  foreign key (asiakas_id) references asiakas(id)
  on delete restrict
);

create table tilausrivi (
  tilaus_id int not null,
  index tilaus_id(tilaus_id),
  foreign key (tilaus_id) references tilaus(id)
  on delete restrict,
  tuote_id int not null,
  index tuote_id(tuote_id),
  foreign key (tuote_id) references tuote(id)
  on delete restrict,
  maara smallint unsigned not null
);