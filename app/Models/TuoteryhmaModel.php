<?php namespace App\Models;

use CodeIgniter\Model;

/**
 * Sisältää asiakas-taulun käsittelyyn liittyviä metodeja.
 */
class TuoteryhmaModel extends Model {
  protected $table = 'tuoteryhma'; // Malli käsittelee tuoteryhma-taulua tietokannassa.
  // Luettelo niistä kentistä, joita päivitetään, kun ajetaan tallennus (esim. save) tietokantaan.
  protected $allowedFields = ['nimi'];


  /**
   * Hakee tuoteryhmät.
   * 
   * @return Array Tuoteryhmät taulukossa.
   */
  public function haeTuoteryhmat() {
    return $this->findAll();
  }

  /**
   * Hakee tuotteen
   * 
   * @param int $id Haettavan tuotteen id.
   * @return Array Haetun tuoteryhmän tiedot taulukkona (yksi rivi).
   */
  public function hae($id) {
    return $this->getWhere(['id' => $id])->getRowArray();
  }

  /**
   * Hakee ensimmäisen tuoteryhmän tietokannassa.
   */
  public function haeEnsimmainenTuoteryhma() {
    $this->select('id');
    $this->orderBy('id','asc');
    $this->limit(1);
    $query = $this->get();
    $tuoteryhma = $query->getRowArray();
    return $tuoteryhma['id'];
  }

  /**
   * Poistaa tuoteryhmän.
   * 
   * @param int $id Poistettavan tuoteryhmän id.
   */
  public function poista($id) {
    $this->where('id',$id);
    $this->delete();
  }
}