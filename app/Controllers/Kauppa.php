<?php namespace App\Controllers;

use App\Models\TuoteryhmaModel;
use App\Models\TuoteModel;
use App\Models\OstoskoriModel;

/**
 * Verkkokauppa-toiminnallisuuden käsittelijä.
 */
class Kauppa extends BaseController
{
  private $tuoteryhmaModel=null;
	private $tuoteModel=null;
	private $ostoskoriModel = null;

	function __construct()
  {
    $this->tuoteryhmaModel = new TuoteRyhmaModel();
		$this->tuoteModel = new TuoteModel();
		$this->ostoskoriModel = new OstoskoriModel();
  }


	/**
	 * Näyttää tuoteryhmän mukaiset tuotteet.
	 */
	public function index($tuoteryhma_id)
	{
    $data['tuoteryhmat'] = $this->tuoteryhmaModel->haeTuoteryhmat();
		$data['tuotteet'] = $this->tuoteModel->haeTuoteRyhmalla($tuoteryhma_id);
		$data['ostoskori_lkm'] = $this->ostoskoriModel->lukumaara();
		echo view('templates/header',$data);
		echo view('kauppa');
		echo view('templates/footer');
	}


	/**
	 * Näyttää yksittäisen tuotteen.
	 */
  public function tuote($tuote_id) {
    $data['tuoteryhmat'] = $this->tuoteryhmaModel->haeTuoteryhmat();
		$data['tuote'] = $this->tuoteModel->hae($tuote_id);
		$data['ostoskori_lkm'] = $this->ostoskoriModel->lukumaara();
		echo view('templates/header',$data);
		echo view('tuote',$data);
		echo view('templates/footer');
  }

	//--------------------------------------------------------------------

}
