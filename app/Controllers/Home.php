<?php namespace App\Controllers;

use App\Models\TuoteryhmaModel;
use App\Models\TuoteModel;
use App\Models\OstoskoriModel;

/**
 * Oletuskäsittelijä.
 */
class Home extends BaseController
{
	private $tuoteryhmaModel=null;
	private $tuoteModel=null;
	private $ostoskoriModel=null;

	function __construct()
  {
		$this->tuoteryhmaModel = new TuoteryhmaModel();
		$this->tuoteModel = new TuoteModel();
		$this->ostoskoriModel = new OstoskoriModel();
  }


	/**
	 * Näyttää sovelluksen etusivun.
	 */
	public function index()
	{
		$data['tuoteryhmat'] = $this->tuoteryhmaModel->haeTuoteryhmat();
		$data['tuotteet'] = $this->tuoteModel->haeKolmeSatunnaistaTuotetta();
		$data['ostoskori_lkm'] = $this->ostoskoriModel->lukumaara();
		echo view('templates/header',$data);
		echo view('etusivu');
		echo view('templates/footer');
	}

	/**
	 * Etsii annetun nimen perustella tuotteita ja näyttää etusivun.
	 */
	public function etsi() {
		$nimi = $this->request->getPost('etsi');
		$data['tuoteryhmat'] = $this->tuoteryhmaModel->haeTuoteryhmat();
		$data['tuotteet'] = $this->tuoteModel->haeNimella($nimi);;
		$data['ostoskori_lkm'] = $this->ostoskoriModel->lukumaara();
		echo view('templates/header',$data);
		echo view('etusivu');
		echo view('templates/footer');
	}

  public function yhteystiedot() {
		echo "TODO";
	}
	//--------------------------------------------------------------------

}
